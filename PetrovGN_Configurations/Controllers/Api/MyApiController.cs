﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PetrovGN_Configurations.Models;
using System.Threading.Tasks;

namespace PetrovGN_Configurations.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyApiController : ControllerBase
    {
        private readonly ILogger<MyApiController> _logger;
        public MyApiController(ILogger<MyApiController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get ordinary thing
        /// </summary>
        /// <remarks>
        ///  
        ///     will return this: 
        ///     
        ///     {
        ///        "intValue": 123,
        ///        "stringValue": "Item1"
        ///     }
        /// 
        /// </remarks>
        /// <returns></returns>
        [HttpGet("GetSomething")]
        public async Task<SomeResult> GetSomething()
        {
            _logger.LogWarning("Thing");
            return new SomeResult() { IntValue = 1, StringValue = "some string" };
        }

        /// <summary>
        /// Get bad thing
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAnotherThing")]
        public async Task<string> GetAnotherThing() {
            return "Bad thing";
        }
    }
}
