using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetrovGN_Configurations
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
             var mapper = new Dictionary<string, string>()
             {
                 { "-SuperArgConfig", "ArgConfig" },
             };
             return Host.CreateDefaultBuilder(args)
                        .ConfigureAppConfiguration((hostingContext, config) =>
                        {
                            config.Sources.Clear();

                            var env = hostingContext.HostingEnvironment;

                            config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                                  .AddJsonFile($"appsettings.{env.EnvironmentName}.json",
                                                 optional: true, reloadOnChange: true);

                            config.AddJsonFile("SuperConfig.json", optional: true, reloadOnChange: true)
                                  .AddJsonFile($"SuperConfig.{env.EnvironmentName}.json",
                                                 optional: true, reloadOnChange: true);

                            config.AddEnvironmentVariables();

                            if (args != null)
                            {
                                config.AddCommandLine(args, mapper);
                            }
                        })
                        .ConfigureWebHostDefaults(webBuilder =>
                        {
                            webBuilder.UseStartup<Startup>();
                        })
                        .ConfigureLogging((hostingContext, builder) => {
                            var env = hostingContext.HostingEnvironment;
                            builder.SetMinimumLevel(LogLevel.Trace);
                            builder.AddLog4Net($"log4net.{env.EnvironmentName}.config");
                        });

        }
            
    }
}
