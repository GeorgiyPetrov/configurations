﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetrovGN_Configurations.Middlewares
{
    public class LiggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LiggingMiddleware> _logger;

        public LiggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<LiggingMiddleware>();
        }

        public async Task InvokeAsync(HttpContext context)
        {
            _logger.LogInformation($"{UriHelper.GetDisplayUrl(context.Request)}");
            await _next(context);
        }
    }


    public static class LiggingMiddlewareExtension
    {
        public static IApplicationBuilder UseLoggingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LiggingMiddleware>();
        }
    }
}
