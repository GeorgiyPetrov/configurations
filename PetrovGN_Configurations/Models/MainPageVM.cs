﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetrovGN_Configurations.Models
{
    /// <summary>
    /// Data for Home Index
    /// </summary>
    public class MainPageVM
    {
        public string StringProp { get; set; }
        public int IntProp { get; set; }
        public string SuperConfig { get; set; }
        public string ArgsConfig { get; set; }
    }
}
