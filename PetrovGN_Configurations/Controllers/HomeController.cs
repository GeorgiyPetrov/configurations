﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PetrovGN_Configurations.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PetrovGN_Configurations.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;
        public HomeController(ILogger<HomeController> logger,
            IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            var model = new MainPageVM()
            {
                StringProp = _configuration.GetValue<string>("SuperStringValue"),
                IntProp = _configuration.GetValue<int>("SuperIntValue"),
                SuperConfig = _configuration.GetValue<string>("SuperConfig"),
                ArgsConfig = _configuration.GetValue<string>("ArgConfig"),
            };
            _logger.LogWarning("Is error");
            _logger.LogInformation($"StringProp {model.StringProp} | IntProp {model.IntProp}");
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
