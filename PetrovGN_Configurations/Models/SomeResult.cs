﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetrovGN_Configurations.Models
{
    /// <summary>
    /// Some result to send as response
    /// </summary>
    public class SomeResult
    {
        /// <summary>
        ///  First Int Value
        /// </summary>
        public int IntValue { get; set; }

        /// <summary>
        /// Second string value
        /// </summary>
        public string StringValue { get; set; }
    }
}
